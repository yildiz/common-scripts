#!/bin/sh
# Watch users memory use and kill highest consuming process if passed threshold
# After running this script, in another shell you can monitor log file
#    tail -f /tmp/$USER/memory_watcher.log

# Get total server memory in Kb
TOTAL_MEMORY=$(free | grep Mem: | awk '{print $2}')

# Define threshold percentage after which we start killing processes
THRESHOLD_PERCENT=10

# Log file
logfile=/tmp/$USER/memory_watcher.log
mkdir -p /tmp/$USER/

while true;
do
  date
  #dump informaton about processes
  USER_PROCESSES=$(ps -u $USER -o rss,pid,command)

  # Total user memory in Kb
  USER_MEMORY=$(echo "$USER_PROCESSES" | awk '{sum+=$1} END {print sum}')

  # Get memory percentage
  MEMORY_PERCENTAGE=$(python -c "print(int( $USER_MEMORY / $TOTAL_MEMORY * 100))")
  echo ""
  echo "user/total= $USER_MEMORY/$TOTAL_MEMORY = $MEMORY_PERCENTAGE%"
  echo ""

  if [ $MEMORY_PERCENTAGE -ge $THRESHOLD_PERCENT ]
  then
    # Find process with highest memory use
    HIGHEST_PROCESS=$(echo "$USER_PROCESSES" | sort -nr | head -n 1)
    HIGHEST_PID=$(echo $HIGHEST_PROCESS |   awk '{print $2}')
    echo "You passed $THRESHOLD_PERCENT% of memory, killing highest using process:"
    echo $HIGHEST_PROCESS
    kill $HIGHEST_PID
  fi
  sleep 120
  echo ""

done > $logfile

