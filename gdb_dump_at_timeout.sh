#!/bin/bash

timeout=200

athenaHLT.py -i -M -ul --imf --threads=1 --concurrent-events=1 --nprocs=1 --number-of-events=1 -C "from AthenaCommon.AppMgr import ServiceMgr; ServiceMgr.HltEventLoopMgr.dbConnIdleWaitSec=0"  --file=/cvmfs/atlas-nightlies.cern.ch/repo/data/data-art/TrigP1Test/data17_13TeV.00327265.physics_EnhancedBias.merge.RAW._lb0100._SFO-1._0001.1 AthExHelloWorld/HelloWorldOptions.py < trans > athhlt.log 2> athhlt.err &

togrep="athenaHLT.py"
i=0
while [ $i -lt $timeout ]
do
    #ps aux| grep $togrep |grep -v grep
    pids=$(ps aux|grep $togrep |grep cyildiz |grep -v grep |awk '{print $2}')

    #echo "Pids to gstack: " $pids
    
    if [ "$pids" == "" ]
    then
      echo "No $togrep process left running. Exiting with 0"
      exit 0
    fi  

    i=$((i+1))
    sleep 1
done

# Timeout reached, dump stack trace and kill all processes
echo "Timeout reached, dumping stack trace and killing processes"
pids=$(ps aux|grep $togrep |grep -v grep |awk '{print $2}')
for pid in $pids; do gstack $pid > stack_$pid; done 
#ps aux| grep $togrep 
for pid in $pids; do kill -9 $pid; done 
exit 1

