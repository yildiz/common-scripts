#!/usr/bin/env sh
# run as 
# ./pingstuff mag001 mag002 mag003
hosts=${@:-mix001 mix002 mix003}

tmpdir=/tmp/$USER/ping

mkdir -p $tmpdir
rm -f $tmpdir/*

for i in $hosts
do 
  # Run pings on the background
  ping -w 1 -c 1 $i |grep statistics -A 1 |grep  " 100% packet" > $tmpdir/${i}.ping &
done

sleep 1

for i in $hosts
do 
  result="OK"

  # Check if there is dns resolution for host
  host $i > /dev/null 2> /dev/null
  if [ $? -ne 0 ]
  then
    result="HOST NOT FOUND"
  elif [ -s /tmpdir/${i}.ping ]  # file not empty, ping failed
  then
    result="NO PING"
  fi
  printf "%20s: $result\n" $i
done
